// --------------------------------------------------------------------------------------
// FILE: jykOpenGLObject.h
//
// This is a class, intended to be both functional and a tutorial of sorts, for
// representing objects (such as the camera) in OpenGL. The class is intended to be
// a plug-and-play solution. I've kept the code as simple as possible, and the only
// dependency is jyk::Vector3<>, which you can replace with any vector class which
// supports the usual operations. (You may have to replace the function name
// NormalizeSelf() with Normalize(), which seems to be what most people use.)
//
// 3. 6DOF
//
// Gimbal-lock free motion with yaw, pitch and roll about the object's local axes.
//
// Most of the class should be fairly self-explanatory, but here are a few notes.
//
// First, it is the +z rather than -z axis that is considered to be forward. When setting
// the view matrix, a 180-degree rotation is applied as the very last transformation
// to align with the -z axis, as per OpenGL convention. This is why the first and third
// rows of the view matrix are negated in GetViewMatrix().
//
// Also, the Move() function is provided as a (rather crude) convenience, so that you
// can use the class 'right out of the box'. Just send the appropriate boolean values
// (for example, derived from user input), the desired speeds, and the time step.
//
// If you have comments, suggestions, or other features that you would like to see
// included, please feel free to contact me through gamedev.net, or at jyk@sunflower.com.
//
// IMPORTANT DISCLAIMER:
//
// At the time of this writing (7-25-05) the class has not been thoroughly tested. I've
// tested it in camera mode, but haven't tried the 'model matrix' functions yet. I also
// haven't tested the lookat function. Please report any problems or bugs you encounter;
// once everything has been tested and has checked out, I'll remove this disclaimer.
// --------------------------------------------------------------------------------------

#ifndef JYKOPENGLOBJECT_H
#define JYKOPENGLOBJECT_H

#include "jykVector3.h"

namespace jyk {

class OpenGLObject
{
public:
    enum {MODE_6DOF};
         
    OpenGLObject();
   
    int GetMode() const;
    Vector3<> GetPos() const;
    Vector3<> GetSide() const;
    Vector3<> GetUp() const;
    Vector3<> GetForward() const;
    Vector3<> GetUpMove() const;
    Vector3<> GetForwardMove() const;

    void Identity();

    void SetPos(const Vector3<>& pos);  
   
    void ApplyYaw(float angle);
    void ApplyPitch(float angle);
    void ApplyRoll(float angle);
   
    void MoveAlongSideAxis(float t);
    void MoveAlongUpAxis(float t);
    void MoveAlongForwardAxis(float t);
   
    void LookAt(const Vector3<>& pos, const Vector3<>& target, const Vector3<>& up);
   
    void SetOpenGLModelMatrix();
    void SetOpenGLViewMatrix();
    void MultOpenGLModelMatrix();
    void MultOpenGLViewMatrix();
   
    void GetModelMatrix(float m[16]) const;
    void GetViewMatrix(float m[16]) const;
   
    void Move(
        bool yawLeft,     bool yawRight,
        bool pitchDown,   bool pitchUp,
        bool rollRight,   bool rollLeft,
        bool moveLeft,    bool moveRight,
        bool moveUp,      bool moveDown,
        bool moveForward, bool moveBack,
        float yawSpeed,   float pitchSpeed, float rollSpeed,
        float sideSpeed,  float upSpeed,    float forwardSpeed,
        float dt);

private:

    void UpdateFromPitchYaw();
    void Orthonormalize();
    float DegToRad(float angle);
    float RadToDeg(float angle);

    int         m_mode;
   
    Vector3<>   m_pos;

    Vector3<>   m_side;
    Vector3<>   m_up;
    Vector3<>   m_forward;
   
    Vector3<>   m_upMove;
    Vector3<>   m_forwardMove;
   
    float       m_pitch;
    float       m_yaw;
};

} // namespace jyk

#endif

// --------------------------------------------------------------------------------------
// FILE: jykOpenGLObject.cpp
// --------------------------------------------------------------------------------------

#include "jykOpenGLObject.h"
#include "gl.h"
#include <cmath>

namespace jyk {

// --------------------------------------------------------------------------------------
OpenGLObject::OpenGLObject()
{
    m_mode = MODE_6DOF;
    Identity();
}
 
// --------------------------------------------------------------------------------------
Vector3<> OpenGLObject::GetPos() const {return m_pos;}
// --------------------------------------------------------------------------------------
Vector3<> OpenGLObject::GetSide() const {return m_side;}
// --------------------------------------------------------------------------------------
Vector3<> OpenGLObject::GetUp() const {return m_up;}
// --------------------------------------------------------------------------------------
Vector3<> OpenGLObject::GetForward() const {return m_forward;}
// --------------------------------------------------------------------------------------
Vector3<> OpenGLObject::GetUpMove() const {return m_upMove;}
// --------------------------------------------------------------------------------------
Vector3<> OpenGLObject::GetForwardMove() const {return m_forwardMove;}
// --------------------------------------------------------------------------------------
void OpenGLObject::SetPos(const Vector3<>& pos) {m_pos = pos;}
// --------------------------------------------------------------------------------------
void OpenGLObject::Identity()
{
    m_pos.Set(0.0f, 0.0f, 0.0f);
    m_side.Set(1.0f, 0.0f, 0.0f);
    m_up.Set(0.0f, 1.0f, 0.0f);
    m_forward.Set(0.0f, 0.0f, 1.0f);
    m_upMove = m_up;
    m_forwardMove = m_forward;
    m_pitch = 0.0f;
    m_yaw = 0.0f;
}
// --------------------------------------------------------------------------------------
void OpenGLObject::ApplyYaw(float angle)
{
  // Rotate side and forward about up
 
  float s = std::sinf(DegToRad(angle));
  float c = std::cosf(DegToRad(angle));
 
  Vector3<> forward = c * m_forward + s * m_side;
  Vector3<> side = c * m_side - s * m_forward;

  m_side = side;
  m_forward = forward;

  m_upMove = m_up;
  m_forwardMove = m_forward;
 
  Orthonormalize();
}
// --------------------------------------------------------------------------------------
void OpenGLObject::ApplyPitch(float angle)
{
  // Rotate up and forward about side
 
  float s = std::sinf(DegToRad(angle));
  float c = std::cosf(DegToRad(angle));
 
  Vector3<> up = c * m_up + s * m_forward;
  Vector3<> forward = c * m_forward - s * m_up;
 
  m_up = up;
  m_forward = forward;

  m_upMove = m_up;
  m_forwardMove = m_forward;
 
  Orthonormalize();
}
// --------------------------------------------------------------------------------------
void OpenGLObject::ApplyRoll(float angle)
{
  // Rotate side and up about forward
 
  float s = std::sinf(DegToRad(angle));
  float c = std::cosf(DegToRad(angle));
 
  Vector3<> side = c * m_side + s * m_up;
  Vector3<> up = c * m_up - s * m_side;
 
  m_side = side;
  m_up = up;

  m_upMove = m_up;
  m_forwardMove = m_forward;
 
  Orthonormalize();
}
// --------------------------------------------------------------------------------------
void OpenGLObject::MoveAlongSideAxis(float t)
{
    m_pos += m_side * t;
}
// --------------------------------------------------------------------------------------
void OpenGLObject::MoveAlongUpAxis(float t)
{
    m_pos += m_upMove * t;
}
// --------------------------------------------------------------------------------------
void OpenGLObject::MoveAlongForwardAxis(float t)
{
    m_pos += m_forwardMove * t;
}
// --------------------------------------------------------------------------------------
void OpenGLObject::LookAt( const Vector3<>& pos, const Vector3<>& target, const Vector3<>& up )
{
    m_forward = target - pos;
    m_forward.NormalizeSelf();
    m_side = up.Cross(m_forward);
    m_side.NormalizeSelf();
    m_up = m_forward.Cross(m_side);
    m_pos = pos;
   
    m_upMove = m_up;
    m_forwardMove = m_forward;
}
// --------------------------------------------------------------------------------------
void OpenGLObject::SetOpenGLModelMatrix()
{
    float m[16];
    GetModelMatrix(m);
    glLoadMatrixf(m); 
}
// --------------------------------------------------------------------------------------
void OpenGLObject::SetOpenGLViewMatrix()
{
    float m[16];
    GetViewMatrix(m);
    glLoadMatrixf(m); 
}
// --------------------------------------------------------------------------------------
void OpenGLObject::MultOpenGLModelMatrix()
{
    float m[16];
    GetModelMatrix(m);
    glMultMatrixf(m); 
}
// --------------------------------------------------------------------------------------
void OpenGLObject::MultOpenGLViewMatrix()
{
    float m[16];
    GetViewMatrix(m);
    glMultMatrixf(m); 
}
// --------------------------------------------------------------------------------------
void OpenGLObject::GetModelMatrix(float m[16]) const
{
    m[0] =  m_side[0];
    m[1] =  m_side[1];
    m[2] =  m_side[2];
    m[3] =  0.0f;
   
    m[4] =  m_up[0];
    m[5] =  m_up[1];
    m[6] =  m_up[2];
    m[7] =  0.0f;
   
    m[8] =  m_forward[0];
    m[9] =  m_forward[1];
    m[10] = m_forward[2];
    m[11] = 0.0f;
   
    m[12] = m_pos[0];
    m[13] = m_pos[1];
    m[14] = m_pos[2];
    m[15] = 1.0f;
}
// --------------------------------------------------------------------------------------
void OpenGLObject::GetViewMatrix(float m[16]) const
{
    m[0] =  -m_side[0];
    m[1] =   m_up[0];
    m[2] =  -m_forward[0];
    m[3] =   0.0f;
   
    m[4] =  -m_side[1];
    m[5] =   m_up[1];
    m[6] =  -m_forward[1];
    m[7] =   0.0f;
   
    m[8] =  -m_side[2];
    m[9] =   m_up[2];
    m[10] = -m_forward[2];
    m[11] =  0.0f;
   
    m[12] =  m_pos.Dot(m_side);
    m[13] = -m_pos.Dot(m_up);
    m[14] =  m_pos.Dot(m_forward);
    m[15] =  1.0f;
}
// --------------------------------------------------------------------------------------
void OpenGLObject::UpdateFromPitchYaw()
{
    float sp = std::sinf(DegToRad(m_pitch));
    float cp = std::cosf(DegToRad(m_pitch));
    float sy = std::sinf(DegToRad(m_yaw));
    float cy = std::cosf(DegToRad(m_yaw));
   
    m_side.Set(cy, 0.0f, -sy);
    m_forward.Set(sy * cp, -sp, cy * cp);
    m_up = m_forward.Cross(m_side);
   
    m_upMove.Set(0.0f, 1.0f, 0.0f);
    m_forwardMove = m_forward;
}
// --------------------------------------------------------------------------------------
void OpenGLObject::Orthonormalize()
{
    m_forward.NormalizeSelf();
    m_side = m_up.Cross(m_forward);
    m_side.NormalizeSelf();
    m_up = m_forward.Cross(m_side);
}
// --------------------------------------------------------------------------------------
void OpenGLObject::Move(
    bool yawLeft,     bool yawRight,
    bool pitchDown,   bool pitchUp,
    bool rollRight,   bool rollLeft,
    bool moveLeft,    bool moveRight,
    bool moveUp,      bool moveDown,
    bool moveForward, bool moveBack,
    float yawSpeed,   float pitchSpeed, float rollSpeed,
    float sideSpeed,  float upSpeed,    float forwardSpeed,
    float dt)
{
    if (yawLeft)
        ApplyYaw(yawSpeed * dt);
    if (yawRight)
        ApplyYaw(-yawSpeed * dt);
    if (pitchDown)
        ApplyPitch(pitchSpeed * dt);
    if (pitchUp)
        ApplyPitch(-pitchSpeed * dt);
    if (rollRight)
        ApplyRoll(rollSpeed * dt);
    if (rollLeft)
        ApplyRoll(-rollSpeed * dt);
    if (moveLeft)
        MoveAlongSideAxis(sideSpeed * dt);
    if (moveRight)
        MoveAlongSideAxis(-sideSpeed * dt);
    if (moveUp)
        MoveAlongUpAxis(upSpeed * dt);
    if (moveDown)
        MoveAlongUpAxis(-upSpeed * dt);
    if (moveForward)
        MoveAlongForwardAxis(forwardSpeed * dt);
    if (moveBack)
        MoveAlongForwardAxis(-forwardSpeed * dt);
}
// --------------------------------------------------------------------------------------
float OpenGLObject::DegToRad(float angle)
{
    const float DEG_TO_RAD = (std::atanf(1.0f) * 4.0f) / 180.0f;
    return angle * DEG_TO_RAD;
}
// --------------------------------------------------------------------------------------
float OpenGLObject::RadToDeg(float angle)
{
    const float DEG_TO_RAD = 180.0f / (std::atanf(1.0f) * 4.0f);
    return angle * DEG_TO_RAD;
}
// --------------------------------------------------------------------------------------

} // namespace jyk


////////////////////////////////////////////////////////////////////////////////////////////////

// issu de http://www.gamedev.net/topic/625618-spaceship-movement/

void IMovable::accelerateForward( float scaleFactor )
{
    float Matrix[16];
    Quaternion q;
   
    // calculates Quaternions from our heading and pitch into _qHeading and _qPitch
    _qPitch.CreateFromAxisAngle(1.0f, 0.0f, 0.0f, _pitch);
    _qHeading.CreateFromAxisAngle(0.0f, 1.0f, 0.0f, _heading);

    q = _qPitch * _qHeading; // Combine the 2 vectors and rotations - Order specific multiplication!
    q.CreateMatrix(Matrix);  // Store our new vector and its rotation
    glMultMatrixf(Matrix);  // This is the same as glRotatef
   
    _qPitch.CreateMatrix(Matrix);      // Load this new Matrix into _qPitch
    _localVelocity.y += Matrix[9] * _acceleration * scaleFactor;  // Location [9] will always contain our Y movemet
    q = _qHeading * _qPitch;       // Combine the Quarts again to get our X and Z vectors
    q.CreateMatrix(Matrix);        // Store our new vector and its rotation
    _localVelocity.x += Matrix[8] * _acceleration * scaleFactor;  // Location [8] will always contain our X movemet
    _localVelocity.z += -Matrix[10] * _acceleration * scaleFactor; // Location [10] will always contain our Z movemet (negate Z in OpenGL)
}


void Quaternion::CreateFromAxisAngle(float x, float y, float z, float degrees)
{
    // First we want to convert the degrees to radians
    // since the angle is assumed to be in radians
    float angle = degreesToRadians(degrees);
    // Here we calculate the sin( theta / 2) once for optimization
    float result = (float)sin( angle / 2.0f );
     
    // Calcualte the w value by cos( theta / 2 )
    _w = (float)cos( angle / 2.0f );
    // Calculate the x, y and z of the quaternion
    _x = float(x * result);
    _y = float(y * result);
    _z = float(z * result);
}

void Quaternion::CreateMatrix(float *pMatrix)
{
    // Make sure the matrix has allocated memory to store the rotation data
    if(!pMatrix) return;
    // First row
    pMatrix[ 0] = 1.0f - 2.0f * ( _y * _y + _z * _z );
    pMatrix[ 1] = 2.0f * (_x * _y + _z * _w);
    pMatrix[ 2] = 2.0f * (_x * _z - _y * _w);
    pMatrix[ 3] = 0.0f;
    // Second row
    pMatrix[ 4] = 2.0f * ( _x * _y - _z * _w );
    pMatrix[ 5] = 1.0f - 2.0f * ( _x * _x + _z * _z );
    pMatrix[ 6] = 2.0f * (_z * _y + _x * _w );
    pMatrix[ 7] = 0.0f;
    // Third row
    pMatrix[ 8] = 2.0f * ( _x * _z + _y * _w );
    pMatrix[ 9] = 2.0f * ( _y * _z - _x * _w );
    pMatrix[10] = 1.0f - 2.0f * ( _x * _x + _y * _y );
    pMatrix[11] = 0.0f;
    // Fourth row
    pMatrix[12] = 0;
    pMatrix[13] = 0;
    pMatrix[14] = 0;
    pMatrix[15] = 1.0f;
    // Now pMatrix[] is a 4x4 homogeneous matrix that can be applied to an OpenGL Matrix
}


/////////////

// issu de http://forum.unity3d.com/threads/73406-6DOF-banking-and-pesky-gimbal-lock/page2

var pitchYawSpeed = 120;
var rotationSpeed = 120;
var bankingSpeed = 8;
 
private var bankingAngle : float;
private var mouseX : float;
private var mouseY : float;
 
function Update () {
    // Normalized mouse input
    mouseX = ((Input.mousePosition.x - Screen.width / 2.0) / (Screen.width / 2.0));
    mouseY = ((Input.mousePosition.y - Screen.height / 2.0) / (Screen.height / 2.0));
    
    // Pitch/Yaw ship
    transform.Rotate(-Vector3.right * (mouseY * pitchYawSpeed) * Time.deltaTime);
    transform.Rotate(Vector3.up * (mouseX * pitchYawSpeed) * Time.deltaTime);
    
    // Rotate ship
    transform.Rotate(Vector3.forward, Input.GetAxis("Rotate") * rotationSpeed * Time.deltaTime);
    
    // Banking effect
    bankingAngle = Mathf.LerpAngle(bankingAngle,  -mouseX * 15, Time.deltaTime * bankingSpeed );
    Camera.main.transform.localEulerAngles.z = bankingAngle;
}

