#include <irrlicht.h>
#include <iostream>
#include <string>
#include <vector>

#ifdef USE_LEAP
#include "Leap.h"
#endif


// TODO: 
// - IA ennemi + tirs ennemis avec couleur changeante
// - fleches pour indiquer les directions des ennemis et des tirs ennemis. couleur des fleches doit changer en fonction de la distance
// - poussieres dans l'espace pour donner l'impression de vitesse

using namespace irr;

#ifdef USE_LEAP
using namespace Leap;
#endif

video::IVideoDriver* driver = NULL;
scene::ISceneManager* smgr = NULL;
gui::IGUIEnvironment* env = NULL;
gui::IGUIFont* font = NULL;

scene::IMeshSceneNode* laserPoint = NULL;
scene::IMeshSceneNode* fleche = NULL;

class VaisseauEnnemi;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

scene::IAnimatedMeshSceneNode* loadObject( core::stringc file, float scale, video::SColor *couleur = NULL, int vertexAlpha = 200 )
{
    scene::IAnimatedMeshSceneNode* objet = 0;
    scene::IAnimatedMesh* objetMesh = smgr->getMesh( file );
    if( !objetMesh ) return NULL;
 
    scene::IMeshManipulator *manipulator = smgr->getMeshManipulator();
    manipulator->setVertexColorAlpha( objetMesh, vertexAlpha );
    if( couleur != NULL ) {
		manipulator->setVertexColors( objetMesh, *couleur );
		objetMesh->setMaterialFlag( irr::video::EMF_LIGHTING, false );	// l'objet n'est pas illumine! la couleur est fixe!
		//objetMesh->setMaterialType( irr::video::EMT_TRANSPARENT_ADD_COLOR );
	}
	
    // scale the mesh
    core::matrix4 m;
    m.setScale( core::vector3df( scale, scale, scale ) );
    manipulator->transform( objetMesh, m );

    objet = smgr->addAnimatedMeshSceneNode( objetMesh );
    return objet;
}

scene::IMeshSceneNode* loadStaticObject( core::stringc file, float scale, video::SColor *couleur = NULL, int vertexAlpha = 200 )
{
    scene::IMeshSceneNode* objet = 0;
    scene::IMesh* objetMesh = smgr->getMesh( file );
    if( !objetMesh ) return NULL;
 
    scene::IMeshManipulator *manipulator = smgr->getMeshManipulator();
    manipulator->setVertexColorAlpha( objetMesh, vertexAlpha );
    if( couleur != NULL ) {
		manipulator->setVertexColors( objetMesh, *couleur );
		objetMesh->setMaterialFlag( irr::video::EMF_LIGHTING, false );	// l'objet n'est pas illumine! la couleur est fixe!
		//objetMesh->setMaterialType( irr::video::EMT_TRANSPARENT_ADD_COLOR );
	}
	
    // scale the mesh
    core::matrix4 m;
    m.setScale( core::vector3df( scale, scale, scale ) );
    manipulator->transform( objetMesh, m );

    objet = smgr->addMeshSceneNode( objetMesh );
    return objet;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////


class MyEventReceiver : public IEventReceiver
{
public:
    // This is the one method that we have to implement
    virtual bool OnEvent(const SEvent& event)
    {
        // Remember whether each key is down or up
        if (event.EventType == irr::EET_KEY_INPUT_EVENT)
            KeyIsDown[event.KeyInput.Key] = event.KeyInput.PressedDown;

        return false;
    }

    // This is used to check whether a key is being held down
    virtual bool IsKeyDown(EKEY_CODE keyCode) const
    {
        return KeyIsDown[keyCode];
    }
   
    MyEventReceiver()
    {
        for (u32 i=0; i<KEY_KEY_CODES_COUNT; ++i)
            KeyIsDown[i] = false;
    }

private:
    // We use this array to store the current state of each key
    bool KeyIsDown[KEY_KEY_CODES_COUNT];
};


void coutQuat( std::string debut, core::quaternion q )
{
    std::cout << debut <<" quaternion( "<< q.W <<", "<< q.X <<", "<< q.Y <<", "<< q.Z <<" )"<< std::endl;
}

void coutVec3( std::string debut, core::vector3df v )
{
    std::cout << debut <<" vector( "<< v.X <<", "<< v.Y <<", "<< v.Z <<" )"<< std::endl;
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////

class objet3D
{
public:
	scene::IMeshSceneNode* model;
	core::vector3df pos;
	int type;
};

float distanceDetection = 500.;		// distance en dessous de laquelle on affiche les fleches d'information

// les tirs des vaisseaux!
class shootObject: public objet3D
{
public:
	shootObject( scene::IMeshSceneNode* m, int t, core::vector3df p, core::vector3df v, int l ) { model = (scene::IMeshSceneNode*)m->clone(); model->setVisible(true); type = t; pos = p; vel = v; life = l; maFleche = (scene::IMeshSceneNode*)fleche->clone(); };

	int life;
	core::vector3df vel;
	scene::IMeshSceneNode* maFleche;
};

std::vector<shootObject> shoots;



//////////////////////////////////////////////////////////////////////////////////////////////////////////////


class Vaisseau: public objet3D
{
public:
    Vaisseau() { vel = acc = stabilizedRot = core::vector3df(0,0,0); motionDir = core::vector3df( 0, 0, 1 ); life = 100; model = NULL; };
    ~Vaisseau(){};
   
    bool initModel( core::stringc file, float scale );
    void move( float t ) { vel += acc*t; if( model != NULL ) model->setPosition( model->getPosition() + ( vel*t ) ); };
   

    core::vector3df vel, acc, stabilizedRot;     // velocite, acceleration, rotation offset (position et orientation sont dans le model!!)
    int life;
    int shootType, shootDelay;
    int animIndex;
    scene::IParticleSystemSceneNode* ps;
    scene::IMeshSceneNode* mapSphere;

    float gravity;
    core::vector3df motionDir;
    float currentVelocity;		// deplacement reel par tour!
};

bool Vaisseau::initModel( core::stringc file, float scale )
{
    scene::IMesh* modelMesh = smgr->getMesh( file );
    if( !modelMesh ) return false;
 
    scene::IMeshManipulator *manipulator = smgr->getMeshManipulator();
    //manipulator->setVertexColorAlpha( modelMesh, 10 );    // set the alpha value of all vertices
    
    // scale the mesh
    core::matrix4 m;
    m.setScale( core::vector3df( scale, scale, scale ) );
    manipulator->transform( modelMesh, m );
    
	modelMesh->setMaterialFlag( irr::video::EMF_LIGHTING, false );	// l'objet n'est pas illumine! la couleur est fixe!

    model = smgr->addMeshSceneNode( modelMesh );
    model->addShadowVolumeSceneNode();


    // la sphere transparente entourant le vaisseau pour afficher les tirs ennemis
    mapSphere = smgr->addSphereSceneNode( 8., 16 );
    //mapSphere->setMaterialFlag( video::EMF_LIGHTING, false );
    //mapSphere->setMaterialFlag( video::EMF_BLEND_OPERATION, true );
    
    manipulator->setVertexColorAlpha( mapSphere->getMesh(), 100 );
	manipulator->setVertexColors( mapSphere->getMesh(), video::SColor(100,255,255,255) );    
    
    mapSphere->setMaterialType( video::EMT_TRANSPARENT_ADD_COLOR );
    //mapSphere->setMaterialFlag( video::EMF_LIGHTING, false );
    
    //video::SColor mapColor( 55, 100, 255, 100 );
    //mapSphere = loadObject( "data/sphere.x", 5., &mapColor, 50 );


	// les flammes a l'arriere du vaisseau: 
	// utilise un emetteur de particules
    ps = smgr->addParticleSystemSceneNode( false, model );

    scene::IParticleEmitter* em = ps->createBoxEmitter(
        core::aabbox3d<f32>( -0.5, 0.5, -0.5, 0.5, -1, 0 ), // emitter size
        core::vector3df( 0.0f, 0.0f, -0.1f ),   // initial direction
        80, 100,                             // emit rate
        video::SColor(0,255,255,255),       // darkest color
        video::SColor(0,255,255,255),       // brightest color
        300, 310, 0,                         // min and max age, angle
        core::dimension2df( 0.5f, 0.5f ),         // min size
        core::dimension2df( 1.f, 1.f ));        // max size

    ps->setEmitter(em); // this grabs the emitter
    em->drop(); // so we can drop it here without deleting it

    scene::IParticleAffector* paf = ps->createFadeOutParticleAffector();

    ps->addAffector(paf); // same goes for the affector
    paf->drop();

    ps->setPosition(core::vector3df( 0, 0, -1 ));
    ps->setScale(core::vector3df(1,1,1));
    ps->setMaterialFlag(video::EMF_LIGHTING, false);
    ps->setMaterialFlag(video::EMF_ZWRITE_ENABLE, false);
    ps->setMaterialTexture(0, driver->getTexture("data/fire.bmp"));
    ps->setMaterialType(video::EMT_TRANSPARENT_ADD_COLOR);

    return true;
}


// le heros est global!
Vaisseau heros;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////

enum MotionType { STOP, AVANT, CHERCHE, ATTAQUE, FUITE };


class VaisseauEnnemi : public Vaisseau
{
public:
    VaisseauEnnemi() { motionType = CHERCHE; scale = 0.1; visionField = 0.3; shootPrecision = 10.; foundHeros = false; maFleche = (scene::IMeshSceneNode*)fleche->clone(); };
    ~VaisseauEnnemi(){};
    
	void ia();
	

	int motionType;
	float scale;
	
	float visionField, shootPrecision;
	bool foundHeros;
	bool invisible;		// pas de fleche indiquant sa position!
	core::vector3df lastHerosPos, lastHerosVel, dirToHeros;
	scene::IMeshSceneNode* maFleche;
};


void VaisseauEnnemi::ia()
{
	switch( motionType ){
		case STOP:
			move( 0. );		// arret graduel ??
			break;
			
		case AVANT:
			move( 1. );
			break;
		
		case CHERCHE:
		case ATTAQUE:
		case FUITE:
			dirToHeros = (heros.model->getPosition() - model->getPosition()).normalize();
			core::matrix4 modelTransf = model->getAbsoluteTransformation();
		    core::vector3df currDir( 0, 0, 1 );
			modelTransf.rotateVect( currDir );
			currDir.normalize();

		    float dp = ( currDir.dotProduct( dirToHeros ) - 0.99f ) * 10.;
		    if( fabs(dp) < visionField ){
				foundHeros = true;
				lastHerosPos = heros.model->getPosition();
				lastHerosVel = heros.vel;
				
				core::vector3df predictDirToHeros = (heros.model->getPosition() + heros.motionDir*heros.currentVelocity - model->getPosition()).normalize();
				currDir = currDir.getInterpolated( predictDirToHeros, 0.1 );		// la vitesse a laquelle on peut tourner!?
				currDir.normalize();
				
				modelTransf = modelTransf.buildRotateFromTo( core::vector3df( 0, 0, 1 ), currDir );
				model->setRotation( modelTransf.getRotationDegrees() );
				
				model->setPosition( model->getPosition() + currDir*(heros.currentVelocity/2.) );

				coutVec3( "Trouve ! Nelle Direction: ", currDir );
				
			} else {
				foundHeros = false;
				// devrait tester plusieurs mouvements pour trouver
			}
		
			break;
	}
}

std::vector<VaisseauEnnemi> lesEnnemis;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////


class VaisseauLeapController {
public:
    VaisseauLeapController() { heros = NULL; initCommon(); };
    VaisseauLeapController( Vaisseau *h ) { heros = h; initCommon(); };
    
    void initCommon() { dbgTxt = ""; laserCharge = 0; speed = 100.; speedMax = 180.; speedMin = 10.; lastPalmN = last2PalmN = core::vector3df( 0, -1, 0 ); };
    void setHeros( Vaisseau *h ) { heros = h; };
    core::stringc getDebugTxt() { return dbgTxt; };
   
    void analyzeFrame( f32 frameDeltaTime, MyEventReceiver &receiver );
    void shoot( f32 frameDeltaTime, MyEventReceiver &receiver );
    void moveVaisseau( f32 frameDeltaTime );

	void shield();
	void shootLaser();
	void shootMissiles();
	void continueState();

    Vaisseau *heros;
    core::matrix4 modelRotMat;
#ifdef USE_LEAP
    Controller controller;
    Frame lastFrame;
    Frame refFrame;
#endif    
    core::stringc dbgTxt;
    
    core::vector3df lastPalmN, last2PalmN;

	// defense and attack
    enum shootState{ IDLE, SHIELD, LASER, MISSILE};
    int shieldForce;
    int shootIter;
    int laserCharge;
    
    // inertie du vaisseau..
    float speed;
    float speedMin, speedMax;
};


void VaisseauLeapController::moveVaisseau( f32 frameDeltaTime )
{
    // nouvelle direction a prendre
    core::vector3df newDir( 0, 0, 1 );
    modelRotMat.rotateVect( newDir );

    //coutVec3( "vaisseau direction: ", newDir );
    
    float dp = ( newDir.dotProduct( heros->motionDir ) - 0.99f ) * 10.;
    speed += dp;
    if( speed > speedMax ) speed = speedMax;
    if( speed < speedMin ) speed = speedMin;
    
    heros->motionDir = heros->motionDir.getInterpolated( newDir, 0.8 );        // 0.8: on garde plus de l apart de la direction actuelle!
    heros->motionDir = heros->motionDir.normalize();

    //std::cout << "dp: "<< dp <<", vitesse: "<< speed << std::endl;
    heros->ps->getEmitter()->setDirection( -newDir );						// mise a jour de la direction de l'emetteur a particules
    
    //coutVec3( "position actuelle: ", heros->model->getPosition() );
    heros->currentVelocity = frameDeltaTime*speed;
    heros->model->setPosition( heros->model->getPosition() + heros->motionDir*heros->currentVelocity );
    //coutVec3( "position future: ", heros->model->getPosition() );
}


void VaisseauLeapController::analyzeFrame( f32 frameDeltaTime, MyEventReceiver &receiver )
{
	core::vector3df palmN;
	
#ifdef USE_LEAP
    const Frame frame = controller.frame();
    Vector leapPalmN;

	if( frame.isValid() ) {
		std::cout <<"leap input.."<< std::endl;
		// si on n'a pas detecté de mains, ou si le leap n'a rien renvoyé de nouveau, on suit le mouvement actuel..
		if( frame.hands().isEmpty() || frame.id() == lastFrame.id() ) {
			// TODO: faudrait plutot extrapoler la normale de la paume!
			moveVaisseau( frameDeltaTime );
			return;
		}
		const Hand hand = frame.hands()[0];
		leapPalmN = -hand.palmNormal();
		palmN = core::vector3df( leapPalmN.x, leapPalmN.y, leapPalmN.z );
		
	} else {
#endif
		//std::cout <<"keyboard input.."<< std::endl;
		// compute palmN using keyboard keys
		f64 angx = 0.;
		f64 angz = 0.;
		core::vector3df palmIrr( 0, 1, 0 );

	    if( receiver.IsKeyDown( irr::KEY_UP ) ) angx -= 6.3;
	    if( receiver.IsKeyDown( irr::KEY_DOWN ) ) angx += 6.3;
		palmIrr.rotateYZBy( angx ); 

	    if( receiver.IsKeyDown( irr::KEY_RIGHT ) ) angz -= 10.3;
	    if( receiver.IsKeyDown( irr::KEY_LEFT ) ) angz += 10.3;
		palmIrr.rotateXYBy( angz );
		
		palmN = palmIrr;
#ifdef USE_LEAP
	}
#endif

    float lenYZ = sqrt( palmN.Y*palmN.Y + palmN.Z*palmN.Z );
    float pitch = acos( palmN.Y / lenYZ );
    if( palmN.Z > 0 ) pitch = -pitch;

    float lenXY = sqrt( palmN.Y*palmN.Y + palmN.X*palmN.X );
    float roll = acos( palmN.Y / lenXY );
    if( palmN.X > 0 ) roll = -roll;

    // modelRotMat: matrice de rotation de l'objet actuel
   
    core::quaternion q;

    // d'abord on roll..
    core::vector3df zAxis( 0, 0, 1 );
    modelRotMat.rotateVect( zAxis );
    q.fromAngleAxis( roll/10., zAxis );
    modelRotMat = q.getMatrix() * modelRotMat;
   
    // ..ensuite on pitch
    core::vector3df xAxis( 1, 0, 0 );
    modelRotMat.rotateVect( xAxis );
    q.fromAngleAxis( pitch/5., xAxis );
    modelRotMat = q.getMatrix() * modelRotMat;

    heros->model->setRotation( modelRotMat.getRotationDegrees() );
    
    moveVaisseau( frameDeltaTime );
   
#ifdef USE_LEAP
    lastFrame = frame;
#endif    
    last2PalmN = lastPalmN;
    lastPalmN = palmN;
}


void VaisseauLeapController::shoot( f32 frameDeltaTime, MyEventReceiver &receiver )
{
#ifdef USE_LEAP
    const Frame frame = controller.frame();
    Vector shootDir;
	if( laserCharge > 0 ) laserCharge--;

	if( frame.isValid() ) {
		// si on n'a pas detecté de mains, ou une seule, ou si le leap n'a rien renvoyé de nouveau, on se protege
		if( frame.hands().isEmpty() || frame.hands().count() < 2 ) {
			shield();
			return;
		}
		if( frame.id() == lastFrame.id() ) {
			continueState();
		}
		
		const Hand hand = frame.hands()[1];
		if( hand.fingers().count() == 1 ) {
			// si un seul doigt, on shoot le laser devant
			if( laserCharge <= 0 ) {
				shootObject so( laserPoint, 1, heros->model->getPosition(), heros->motionDir*10., 100 );
				shoots.push_back( so );
				laserCharge = 5;
			}
			
		} else if( hand.fingers().count() >= 4 ){
			// si >4 doigts, on shoot la batterie de missiles a tete chercheuse
			std::cout <<"anarchy shoot !!!!"<< std::endl;
		}
		
	} else {
#endif		
	    if( receiver.IsKeyDown( irr::KEY_KEY_2 ) ) shootMissiles();
	    else if( receiver.IsKeyDown( irr::KEY_KEY_1 ) ) {
			shootObject so( laserPoint, 1, heros->model->getPosition(), heros->motionDir*10., 100 );
			shoots.push_back( so );
		}
		else shield();
#ifdef USE_LEAP
	}
#endif	
}


void VaisseauLeapController::shield()
{
}

void VaisseauLeapController::shootLaser()
{
}

void VaisseauLeapController::shootMissiles()
{
}

void VaisseauLeapController::continueState()
{
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void shooting()
{
	for( std::vector<shootObject>::iterator it = shoots.begin() ; it != shoots.end(); ) {
		it->life--;
		if( it->life <= 0 ) {
			it->model->setVisible( false );
			it->model->removeAll();
			it = shoots.erase( it );
			
		} else {
			it->pos += it->vel;
			it->model->setPosition( it->pos );
			bool tir_detruit = false;
			
			// est ce qu'on touche un vaisseau ennemi?
			for( std::vector<VaisseauEnnemi>::iterator ve = lesEnnemis.begin() ; ve != lesEnnemis.end(); ++ve ) {
				float distance = ve->model->getPosition().getDistanceFrom( it->pos );
				//std::cout << "tir distance: : "<< distance << std::endl;
				if( distance < 20. ) {
					ve->life--;
					std::cout << "Touche !! : "<< ve->life << std::endl;
					
					// on detruit le tir
					it->model->setVisible( false );
					it->model->removeAll();
					it = shoots.erase( it );
					tir_detruit = true;
					break;
				}
			}
			
			if( !tir_detruit ) ++it;
		}
	}
}



void updateArrows( float sphereDist = 8.0 )
{
	// on parcourt la liste des ennemis pour savoir quelles fleches afficher
	for( std::vector<VaisseauEnnemi>::iterator it = lesEnnemis.begin() ; it != lesEnnemis.end(); it++ ) {
		core::vector3df direction = heros.model->getPosition() - it->model->getPosition();
		if( direction.getLength() > distanceDetection ) {
			// l'objet est trop loin, on ne le detecte plus!
			it->maFleche->setVisible( false );
			continue;
		}
		it->maFleche->setVisible( true );
		it->maFleche->setPosition( heros.model->getPosition() - direction.normalize()*sphereDist );
		core::matrix4 matRot;
		it->maFleche->setRotation( matRot.buildRotateFromTo( core::vector3df(0.,0.,1.), direction.normalize() ).getRotationDegrees() );
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void loadPlanet( core::stringc file, float scale, core::vector3df rotSpeed, float radius, float speed )
{
    scene::IAnimatedMeshSceneNode* earth = 0;
    scene::IAnimatedMesh* earthMesh = smgr->getMesh( file );
    if( !earthMesh ) return;
 
    scene::IMeshManipulator *manipulator = smgr->getMeshManipulator();
    manipulator->setVertexColorAlpha( earthMesh, 200 );    // set the alpha value of all vertices to 200

    // scale the mesh
    core::matrix4 m;
    m.setScale( core::vector3df( scale, scale, scale ) );
    manipulator->transform( earthMesh, m );

    earth = smgr->addAnimatedMeshSceneNode( earthMesh );

    // add the shadow
    earth->addShadowVolumeSceneNode();
 
    // add animators
    scene::ISceneNodeAnimator* rotAnim = smgr->createRotationAnimator( rotSpeed );    // degree / 10ms
    earth->addAnimator( rotAnim );
    rotAnim->drop();

    scene::ISceneNodeAnimator* orbitAnim = smgr->createFlyCircleAnimator( core::vector3df(0,0,0), radius, speed );    // speed: radians/ms
    earth->addAnimator( orbitAnim );
    orbitAnim->drop();
}


void loadUniverseSky( core::stringc file, float radius )
{
    driver->setTextureCreationFlag( video::ETCF_CREATE_MIP_MAPS, false );

    scene::ISceneNode* skydome = smgr->addSkyDomeSceneNode( driver->getTexture( file ), 16, 8, 0.99f, 2.0f, radius );
    skydome->setVisible( true );
   
    driver->setTextureCreationFlag( video::ETCF_CREATE_MIP_MAPS, true );
}


void addSunlight( float scale )
{
    // une source de lumiere!
    scene::ILightSceneNode* sunlight = smgr->addLightSceneNode( 0, core::vector3df(0,0,0), video::SColorf(1.0f, 0.95f, 0.8f, 0.8f), scale );
    sunlight->setDebugDataVisible( scene::EDS_BBOX );
 
 
    // ajoute un billboard pour l'effet lumineux
    scene::IBillboardSceneNode* bill = smgr->addBillboardSceneNode( sunlight, core::dimension2d<f32>( scale*0.7, scale*0.7 ) );
    bill->setMaterialFlag( video::EMF_LIGHTING, false );
    bill->setMaterialFlag( video::EMF_ZWRITE_ENABLE, false );
    bill->setMaterialType( video::EMT_TRANSPARENT_ADD_COLOR );
    bill->setMaterialTexture( 0, driver->getTexture("data/particlewhite.bmp") );


    // on ajoute une sphere quand meme..
    scene::IMesh* sunMesh = smgr->getMesh( "data/sun.x" );
    if( !sunMesh ) return;
 
    scene::IMeshManipulator *manipulator = smgr->getMeshManipulator();
    manipulator->setVertexColorAlpha( sunMesh, 200 );    // set the alpha value of all vertices to 200

    // scale the mesh
    core::matrix4 m;
    scale = 15.;    // pour les tests..
    m.setScale( core::vector3df( scale, scale, scale ) );
    manipulator->transform( sunMesh, m );

    scene::IMeshSceneNode* soleil = smgr->addMeshSceneNode( sunMesh );
    if( !soleil ) return;
    soleil->setMaterialFlag( video::EMF_LIGHTING, false );
}



/////////////////////////////////////////////////////////////////////////////////////////////////

int main()
{
    ///////////////////////////////////// initialisation d'Irrlicht 3D
   
    MyEventReceiver receiver;
   
    IrrlichtDevice* device = createDevice( video::EDT_OPENGL, core::dimension2d<u32>(1000, 800), 32, false, true, false, &receiver );
    if( !device ) return 1; // could not create selected driver.

    driver = device->getVideoDriver();
    smgr = device->getSceneManager();
    env = device->getGUIEnvironment();


    /////////////////////////////////////  l'environnement
   
    float yearScale = 0.0001;
    float dayScale = yearScale / 365.0 * 5.;
    float orbitScale = 1000.; //23481.07;
    float sizeScale = 10.0;

    // les planetes..
    // arguments: fichier modele, echelle, vitesse rotation, rayon, vitesse orbitale
    loadPlanet( "data/mercure.x", 0.383 * sizeScale, core::vector3df( 0, 59 * dayScale, 0 ), 0.38 * orbitScale, 0.241 * yearScale );
    loadPlanet( "data/earth.x", sizeScale, core::vector3df( 0, dayScale, 0 ), orbitScale, yearScale );    // sizeScale, , orbitScale

    // le soleil et source de lumière
    addSunlight( 109. * sizeScale );
   
    // le ciel étoilé..
    //loadUniverseSky( "data/skydome.jpg", 3.0 * orbitScale );
    float horizon = 30.0 * orbitScale;
    loadUniverseSky( "data/torea-starfield.png", horizon );


    /////////////////////////////////////  le vaisseau

    heros.initModel( "data/spaceship/Feisar_Ship_3DS/Feisar_Ship2.obj", 0.03 );
    heros.model->setPosition( core::vector3df( 0, 0, -400 ) );
    heros.vel = core::vector3df( 1, 0, 0 );
    heros.acc = core::vector3df( 0, 0, 0 );
   
    core::vector3df lastHerosPos = heros.model->getPosition();
    
    // template pour les fleches de renseignement
    video::SColor flecheColor( 170, 200, 255, 200 );
    fleche = loadStaticObject( "data/fleche.x", 0.6, &flecheColor );	// la fleche de base originale (faire des copies visible pour indiquer ennemi proche ou tir ennemi arrivant dans notre direction!)
    fleche->setVisible( false );

    // template pour les tirs normaux
    video::SColor laserColor( 255, 210, 240, 240 );
    laserPoint = loadStaticObject( "data/icosphere.x", 1., &laserColor );	// le tir laser de base original (faire des copies visible a chaque tir!!)
    laserPoint->setVisible( false );

    /////////////////////////////////////  les ennemis

    VaisseauEnnemi mechant;
    mechant.scale = 0.1;
    mechant.initModel( "data/spaceship/Feisar_Ship_3DS/Feisar_Ship.obj", mechant.scale );
    mechant.model->setPosition( core::vector3df( 0, 200, -300 ) );
    mechant.vel = core::vector3df( 0.6, 0, 0 );
    mechant.acc = core::vector3df( 0, 0, 0 );
    lesEnnemis.push_back( mechant);


    /////////////////////////////////////  les paramètres
    // la camera
    //scene::ICameraSceneNode* camera = smgr->addCameraSceneNodeFPS();
    scene::ICameraSceneNode* camera = smgr->addCameraSceneNode( 0, core::vector3df( -600, 0, 0), heros.model->getPosition() );
    camera->setFarValue( horizon+1. );
 
    // divers
    device->getCursorControl()->setVisible( false );           // on vire le curseur de la souris
    smgr->setShadowColor( video::SColor( 150, 0, 0, 0 ) );      // les ombres
   
    font = device->getGUIEnvironment()->getBuiltInFont();       // la font pour ecrire du texte sur l'ecran
    font->setKerningHeight( 30 );
    font->setKerningWidth( 5 );

    // Leap motion
    VaisseauLeapController vaisseauController( &heros );


    ///////////////////////////////////// c'est parti !!
   
    int lastFPS = -1;
    u32 then = device->getTimer()->getTime();
    int loop20 = -20;
   
    while( device->run() ) {
        if( device->isWindowActive() ) {
            // temps entre les frames
            const u32 now = device->getTimer()->getTime();
            const f32 frameDeltaTime = (f32)(now - then) / 1000.f; // Time in seconds
            then = now;
           
            if( loop20 < 1 ) loop20++;

           
			// notre vaisseau heros
#ifdef USE_LEAP
            if( receiver.IsKeyDown( irr::KEY_KEY_R ) || loop20 < 1 ) {
                vaisseauController.refFrame = vaisseauController.controller.frame();
            }
#endif            
            vaisseauController.analyzeFrame( frameDeltaTime, receiver );
            vaisseauController.shoot( frameDeltaTime, receiver );
            
            if( heros.mapSphere != NULL ) heros.mapSphere->setPosition( heros.model->getPosition() );

            shooting();


			// les ennemis
			for( std::vector<VaisseauEnnemi>::iterator it = lesEnnemis.begin() ; it != lesEnnemis.end(); ) {
				if( it->life <= 0 ) {
					it->model->setVisible( false );
					it->model->removeAll();

					// doit aussi degager la fleche, si activee..
					if( it->maFleche != NULL ) {
						it->maFleche->setVisible( false );
						it->maFleche->removeAll();
					}

					it = lesEnnemis.erase( it );
					
					// devrait lancer l'animation d'une explosion!
				} else {
					it->ia();	// lance les actions et les deplacements pour ce tour!
					++it;
				}
			}
			
			updateArrows();

			
            // bouge la camera en fonction de la position et direction du vaisseau!!
            //camera->setPosition( heros.model->getPosition() + (lastHerosPos - heros.model->getPosition())/frameDeltaTime*5. );
            camera->setPosition( heros.model->getPosition() + (lastHerosPos - heros.model->getPosition()).normalize()*60. );
            camera->setTarget( heros.model->getPosition() );
            core::vector3df herosUp( 0, 1, 0 );
            vaisseauController.modelRotMat.rotateVect( herosUp );
            camera->setUpVector( herosUp );
           
            lastHerosPos = heros.model->getPosition();

            driver->beginScene( true, true, 0 );
           
            smgr->drawAll();
            env->drawAll();

			// TODO: en fait, ca serait mieux d'afficher des fleches 3D!!
            // une aide a la localisation 3D: on dessine les lignes qui vont du vaisseau au soleil et du vaisseau a l'axe Y
			video::SMaterial Material;
			Material.Lighting = true;
			Material.EmissiveColor = video::SColor(200,150,255,150);
			Material.Shininess = 0;
			Material.Thickness = 2.;
			Material.FogEnable = true;
			Material.GouraudShading = false;
			Material.MaterialType = video::EMT_TRANSPARENT_ALPHA_CHANNEL;
			driver->setMaterial( Material );
			driver->setTransform( video::ETS_WORLD, core::IdentityMatrix );
            driver->draw3DLine( lastHerosPos, core::vector3df(0,0,0), video::SColor(50,150,255,150) );
            driver->draw3DLine( lastHerosPos, lastHerosPos + core::vector3df(0,100,0), video::SColor(50,150,255,250) );

            // devrait dessiner des lignes vers tous les vaisseaux ennemis proches
            //driver->draw3DLine( lastHerosPos, core::vector3df(0,1,0), video::SColor(200,150,255,250) );
           
            if( font ) {
                core::stringc dataDispTxt = vaisseauController.getDebugTxt();
                font->draw( dataDispTxt, core::rect<s32>(100,550,780,690), video::SColor(255,100,0,0) );
            }
           
            driver->endScene();
         
            // on affiche les FPS:
            int fps = driver->getFPS();
         
            if( lastFPS != fps ) {
                core::stringw str = L"Solar Fight - Irrlicht Engine [";
                str += driver->getName();
                str += "] FPS:";
                str += fps;
         
                device->setWindowCaption( str.c_str() );
                lastFPS = fps;
            }
        }
    }
 
    device->drop();
   
    return 0;
}

